
const app = {
    data(){
        return {
            usuario : 'Pepet',
            correo: '',
            clave: '',
            nombre: 'Pare'
        }
    },
    methods:{
        llamarSaludoDesdeHijo(){
            this.$refs.menuComponent.saludarDesdeHijo(this.nombre);
            this.$refs.menuComponent.miUsuario = this.nombre;
        }
    }
}

const _app = Vue.createApp(app);


 

/*

//Componentes y Traspaso de Datos
const const14 = {
    data() {
        return {
            nombre: 'Ivan',
            maxComponents: 5,
            posts: []
        }
    },
    created() {
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(response => response.json())
            .then(data => {
                this.posts = data;
            });
    }
};
var _var14 = Vue.createApp(const14);
//Cada component pot tenir metodes i tot lo altre definit fins ara (osigui caa component actua com una app) 
_var14.component('hello-world-14', { //Aquest nom serà l'invocat com un tag html dintre del container de l'APP
    template: `
        <div class="card-panel teal white-text">
            <h5>{{ post.title }}</h5>
            <p> {{ post.body }}</p>
        </div>`,
    props: {
        post: Object
    },
    data() {
        return {
            nombre: 'Ivan14'
        }
    }
});
var var14 = _var14.mount('#container14');
*/


/*
//-- xxx: --
const const3 = {
    data() {
        return {
            nombre: 'Ivan'
        }
    }
};
var var3 = Vue.createApp(const3).mount('#container3');
*/