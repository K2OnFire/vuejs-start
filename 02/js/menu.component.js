_app.component('menu-component', {
    template: `
    <nav class="indigo darken-4">
        <div class="nav-wrapper">
        <a href="#" class="brand-logo">Logo</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="sass.html">Sass</a></li>
            <li><a href="badges.html">Components</a></li>
            <li><a href="collapsible.html">{{ usuario }}</a></li>
        </ul>
        </div>
    </nav>
    <input type="text" v-model="miUsuario">
    <hr/>
    <button type="button" class="green darken-3" v-on:click="saludarDesdeHijo">Saludar directo del hijo</button>`,
    props: {
        usuario: String, //Les propietats son de omes lectura
    },
    data() {
        return {
            miUsuario: this.usuario //Si les volem modificar cal setejar-les com a data en el component
        }
    },
    methods: {
        saludarDesdeHijo(nombre) {
            console.log("saludarDesdeHijo " + nombre);
        }
    }
});