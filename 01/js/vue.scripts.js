
//-- Inici: --
//data, methods, props, mounted, created, computed,  watcher, destroyed
const holaMundo1 = {
    data() {
        return {
            nombre: 'Ivan',
            anyo: 2021,
            meses: ['enero', 'febrero']
        }
    }
};
const holaMundo2 = {
    data() {
        return {
            nombre: 'Ivan'
        }
    }
};

var app1 = Vue.createApp(holaMundo1).mount('#app1');
//Sempre s'utilitza una sola APP, però si es volgues podriem posar-ne més.
var app2 = Vue.createApp(holaMundo2).mount('#app2');




//-- LLamada de metodos y manipulacion de datos: --
const const1 = {
    data() {
        return {
            contador: 0,
            interval: null,
            isRunning: false,
            txtConst1: 'Detener',
        }
    },
    mounted() {
        this.interval = setInterval(() => {
            this.contador++;
            this.isRunning = true;
            this.txtConst1 = 'Detener';
        }, 1000)
    },
    methods: {
        stopInterval() {
            if (this.isRunning) {
                clearInterval(this.interval);
                this.isRunning = false;
                this.txtConst1 = 'Reanudar';
            } else {
                this.interval = setInterval(() => {
                    this.contador++;
                    this.isRunning = true;
                    this.txtConst1 = 'Detener';
                }, 1000)
            }
            console.log("const1: stopInterval()");
        },
    }
};
var var1 = Vue.createApp(const1).mount('#container1');




//-- Binding de Atributos HTML con v-bind: --
const const2 = {
    data() {
        return {
            titulo: 'Este es un titulo dinamico',
            imagen: 'https://pixabay.com/get/g5d322438b98c048315e165cf32740b49f23433471d4d79a0279876685cfa7ceb8a0fb9782d7a56ed2d1cedf725aee128_640.png',
            imagenOld: ''
        }
    },
    methods: {
        mostrarImagen(imagen) {
            this.imagenOld = this.imagen;
            this.imagen = imagen;
        },
        restablecerImagen() {
            this.imagen = this.imagenOld;
        }
    }
};
var var2 = Vue.createApp(const2).mount('#container2');




//Binding de Doble Vía con v-model: Interacción del Usuario
const const3 = {
    data() {
        return {
            nombre: 'Ivan',
            fruta: 'manzana',
            activo: true,
        }
    }
};
var var3 = Vue.createApp(const3).mount('#container3');


//Bucles: Estructura v-for, Recorrido de Arrays y Creación de Listas
const const4 = {
    data() {
        return {
            frutaId: 2,
            frutas: ['naranja', 'piña', 'plátano'],
            frutas_con_id: [
                {
                    id: 1,
                    nombre: 'piñón'
                }, {
                    id: 2,
                    nombre: 'almendra'
                }, {
                    id: 3,
                    nombre: 'pistacho'
                }
            ]
        }
    }
};
var var4 = Vue.createApp(const4).mount('#container4');



//Bucles Anidados con v-for, Obtener Índice, Bucles a Partir de Rangos 
const const5 = {
    data() {
        return {
            letras: 'abcde',
            frutas_con_id: [
                {
                    id: 1,
                    nombre: 'piñón',
                    paises_origen: [
                        {
                            id_pais: 1,
                            pais: "Alemania"
                        }, {
                            id_pais: 2,
                            pais: "España"
                        }
                    ]
                }, {
                    id: 2,
                    nombre: 'almendra',
                    paises_origen: null
                }, {
                    id: 3,
                    nombre: 'pistacho',
                    paises_origen: [
                        {
                            id_pais: 5,
                            pais: "Marruecos"
                        }, {
                            id_pais: 6,
                            pais: "Italia"
                        }
                    ]
                }
            ]
        }
    }
};
var var5 = Vue.createApp(const5).mount('#container5');




//Conexión a API Pública Para Datos de Prueba
//+ Ejemplo de Creación de Tabla
const const6 = {
    data() {
        return {
            usuarios: [],
            posts: [],
            posts_pares: [] //Com que no es pot fer un v-if en el mateix element d'un v-for (pero si que es pot a dins), creem les propietats del for ja filtrades i despres l'iterem
        }
    },
    methods: {
        getUsuarios() {
            fetch('https://jsonplaceholder.typicode.com/users/')
                .then(response => response.json()) //S'han de fer funcions de tipus fletxa varResp => funcio, sino amb les funciona anonimes normals perd la reactivitat
                .then(data => {
                    this.usuarios = data;
                });
        },
        getPosts() {
            fetch('https://jsonplaceholder.typicode.com/posts/')
                .then(response => response.json())
                .then(data => {
                    this.posts = data;
                    this.posts_pares = data.filter(x => (x.id % 2 == 0))
                });
        }
    },
    mounted() {
        this.getUsuarios();
        this.getPosts();
    }
};
var var6 = Vue.createApp(const6).mount('#container6');



//Renderizado Condicional con v-if 
const const7 = {
    data() {
        return {
            numero_uno: 6,
            numero_dos: 5,
            numero_tres: 3,
        }
    }
};
var var7 = Vue.createApp(const7).mount('#container7');



//Manejo de Eventos
//keydown, load, mouseout, mouseover... tots els events estandard
//+ Visibilidad Condicional con v-show. Diferencia con v-if
const const8 = {
    data() {
        return {
            contador: 0,
            estado: false,
            texto: 'texto1'
        }
    },
    methods: {
        cambiarTexto(event) {
            //console.log("Event.target.value: ", event.target.value);
            this.texto = event.target.value;
        }
    }
};
var var8 = Vue.createApp(const8).mount('#container8');



//Manejo de Propiedades "Booleanas" de Etiquetas HTML
const const9 = {
    data() {
        return {
            txtDisabled: false,
            usuarios: 3,
            chkChecked: false,
            txtRequired: true,
            frutas: [
                {
                    id: 1,
                    nombre: "Sandía",
                    selected: false
                }, {
                    id: 2,
                    nombre: "Melón",
                    selected: true
                }, {
                    id: 3,
                    nombre: "Pipa",
                    selected: false
                }
            ]
        }
    },
    methods: {
        toogleInputs() {
            if (this.txtDisabled) {
                this.txtDisabled = false;
            } else {
                this.txtDisabled = true;
            }
            if (this.usuarios == 6) {
                this.usuarios = 3;
            } else {
                this.usuarios = 6;
            }
            if (this.chkChecked) {
                this.chkChecked = false;
            } else {
                this.chkChecked = true;
            }
            if (this.txtRequired) {
                this.txtRequired = false;
            } else {
                this.txtRequired = true;
            }
        }
    }
};
var var9 = Vue.createApp(const9).mount('#container9');


//Enlace de Clases CSS v-bind:class 
const const10 = {
    data() {
        return {
            rounded: true, //aquest al html va amb claus {}
            i: 3,
            roundedClass: 'rounded red btn', //Aquest al html es defineix com un array []
            enableRoundedClass: true, //Per activar condicionalment el grup de classes en array
            myClass: {
                rounded: false, //aquest va sense claus al html, pertant deduim que v-bind:class espera un object on s'hi poden posar multiples classes amb valors booleans
            }
        }
    },
    methods: {
        toogle() {
            if (this.rounded) {
                this.rounded = false;
            } else {
                this.rounded = true;
            }
            if (this.myClass.rounded) {
                this.myClass.rounded = false;
            } else {
                this.myClass.rounded = true;
            }
            if (this.enableRoundedClass) {
                this.enableRoundedClass = false;
            } else {
                this.enableRoundedClass = true;
            }
        }
    }
};
var var10 = Vue.createApp(const10).mount('#container10');


//Enlace de Estilos v-bind:style
const const11 = {
    data() {
        return {
            myStyle: { 'font-size': '16px', backgroundColor: 'green' },
            myStyle2: { 'border': '3px dashed blue' },
            color: 'red',
        }
    }
};
var var11 = Vue.createApp(const11).mount('#container11');



//Propiedades Calculadas
const const12 = {
    data() {
        return {
            n1: 0,
            n2: 0,
            fruta: '',
            frutas: [],
        }
    },
    methods: {
        agregarFruta() {
            this.frutas.push(this.fruta);
            this.fruta = "";
        }
    },
    computed: {
        resultado: function () {
            return parseFloat(this.n1) + parseFloat(this.n2);
        },
        total_frutas: function () {
            return this.frutas.length;
        }
    }
};
var var12 = Vue.createApp(const12).mount('#container12');




//Ciclo de vida de Vue
const const13 = {
    data() {
        return {
            nombre: 'Ivan',
            apiResult: []
        }
    },
    beforeCreate() {
        console.log("const13: beforeCreate(). this.nombre: " + this.nombre);
    },
    created() {
        //Aqui es pot posar el fetch de la crida a la API
        console.log("const13: created(). this.nombre: " + this.nombre);
    },
    beforeMount() {
        console.log("const13: beforeMount(). this.nombre: " + this.nombre);
    },
    mounted() {
        //Aqui per alterar (tenir acces al elements del DOM) les dades de la API.
        console.log("const13: mounted(). this.nombre: " + this.nombre);
    },
    beforeUpdate() {
        console.log("const13: beforeUpdate(). this.nombre: " + this.nombre);
        console.log("const13: beforeUpdate(). #container13: " + document.querySelector('#container13').innerHTML);
    },
    updated() {
        //Per veure els canvis, en consola: var13.nombre="Pepet";
        console.log("const13: updated(). this.nombre: " + this.nombre);
        console.log("const13: updated(). #container13: " + document.querySelector('#container13').innerHTML);
    },
    beforeUnmount() {
        //Per veure els canvis, en consola: _var13.unmount();
        console.log("const13: beforeUnmount(). this.nombre: " + this.nombre);
    },
    unmounted() {
        console.log("const13: unmounted(). this.nombre: " + this.nombre);
    }
};
//var var13 = Vue.createApp(const13).mount('#container13');
var _var13 = Vue.createApp(const13); //Separant en 2 passos, amb _var13 es pot accedir a tota la APP
var var13 = _var13.mount('#container13'); //i el 2n pas per acabar de muntar (en var12 i anteriors ho teniem tot unificat 1 sol pas)



//Componentes y Traspaso de Datos
const const14 = {
    data() {
        return {
            nombre: 'Ivan',
            maxComponents: 5,
            posts: []
        }
    },
    created() {
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(response => response.json())
            .then(data => {
                this.posts = data;
            });
    }
};
var _var14 = Vue.createApp(const14);
//Cada component pot tenir metodes i tot lo altre definit fins ara (osigui caa component actua com una app) 
_var14.component('hello-world-14', { //Aquest nom serà l'invocat com un tag html dintre del container de l'APP
    template: `
        <div class="card-panel teal white-text">
            <h5>{{ post.title }}</h5>
            <p> {{ post.body }}</p>
        </div>`,
    props: {
        post: Object
    },
    data() {
        return {
            nombre: 'Ivan14'
        }
    }
});
var var14 = _var14.mount('#container14');



/*
//-- xxx: --
const const3 = {
    data() {
        return {
            nombre: 'Ivan'
        }
    }
};
var var3 = Vue.createApp(const3).mount('#container3');
*/