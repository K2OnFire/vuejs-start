import { createApp } from 'vue';
import App from './App.vue';

//Materialize
import 'materialize-css/dist/css/materialize.min.css';
import 'material-design-icons/iconfont/material-icons.css';

//Axios
import axios from 'axios';
import VueAxios from 'vue-axios';
//axios.defaults.baseUrl = 'https://jsonplaceholder.typicode.com';
//Amb la linia de dalt, els ajax els podem fer amb /login, /profile ... sense tenir qe escriure la URL HTTP completa
var token = localStorage.getItem('token');
if (token) {
    //Si han recarregat la pagina i ja estavem validats, recupera el token per les futures peticions
    axios.defaults.headers.common["Authorization"] =
        "Bearer " + token;
}

//Vue router
import router from './router';

//Views

//Original
//createApp(App).mount('#app')

//Amb axios (iclourel dins la instància de VUE)
createApp(App).use(router).use(VueAxios, axios).mount('#app')