import { createWebHistory, createRouter } from "vue-router";
//Pagines
import FormTest from '../views/FormTest.vue';
import Login from '../views/Login.vue';
import Estudiantes from '../views/Estudiantes.vue';
//Rutes
const routes = [
    {
        path: '/form-test',
        name: 'FormTest',
        component: FormTest
    }, {
        path: '/login',
        name: 'Login',
        component: Login
    }, {
        path: '/estudiantes',
        name: 'Estudiantes',
        component: Estudiantes
    }, {
        path: '/',
        component: Login
    }, 
];
const router = createRouter(
    {
        history: createWebHistory(),
        routes
    }
);

export default router;